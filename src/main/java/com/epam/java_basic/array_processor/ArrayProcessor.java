package com.epam.java_basic.array_processor;

import java.util.ArrayList;

/**
 *  Useful methods for array processing
 */
public class ArrayProcessor {

    public static int[] swapMaxNegativeAndMinPositiveElements(int[] input1) {
        int [] input = input1.clone();
        int minPositive = Integer.MAX_VALUE;
        int position1 = 0;
        int maxNegative = Integer.MIN_VALUE;
        int position2 = 0;
        for (int i = 0; i < input.length; i++){
            if (input[i] > 0 && input[i]<minPositive){
                minPositive = input[i];
                position1 = i;
            }
            if(input[i] < 0 && input[i] > maxNegative){
                maxNegative = input[i];
                position2 = i;
            }
        }
        input[position1] = maxNegative;
        input[position2] = minPositive;
        return input;
    }

    public static int countSumOfElementsOnEvenPositions(int[] input1) {
        int [] input = input1.clone();
        int sum = 0;
        for (int i = 0; i < input.length; i+=2){
            sum+=input[i];
        }
        return sum;
    }

    public static int[] replaceEachNegativeElementsWithZero(int[] input1) {
        int [] input = input1.clone();
        for(int i = 0; i < input.length; i++){
            if(input[i] < 0){
                input[i] = 0;
            }
        }
        return input;
    }

    public static int[] multiplyByThreeEachPositiveElementStandingBeforeNegative(int[] input1) {
        int [] input = input1.clone();
        for(int i = 0; i < input.length; i++){
            if(i+1 != input.length && input[i] > 0 && input[i+1] < 0){
                input[i] *= 3;
            }
        }

        return input;
    }

    public static float calculateDifferenceBetweenAverageAndMinElement(int[] input1) {
        int [] input = input1.clone();
        int sum = 0;
        int min = Integer.MAX_VALUE;
        for(int i = 0; i < input.length; i++){
            if (input[i] < min){
                min = input[i];
            }
           sum+=input[i];
            }
        float result = ((float) sum/input.length)-min;
        return result;
    }

    public static int[] findSameElementsStandingOnOddPositions(int[] input1) {
        int [] input = input1.clone();
        ArrayList<Integer> base = new ArrayList<>();
        for(int i = 1; i < input.length; i+=2){
            int count = 0;
            for (int j = 0; j < input.length; j++){
                if (input[i] == input[j]) {
                    count++;
                }
                if (count > 1 && j == input.length-1){
                    base.add(input[i]);
                }
            }
        }
        for (int i = 0; i < base.size(); i++){
            int count = 0;
            for(int j = 0; j < base.size();j++){
                if(base.get(i).equals(base.get(j))){
                    count++;
                    if (count > 1) {
                        base.remove(j);
                    }
                }
            }
        }
        int [] result = new int[base.size()];
        for (int i = 0; i < result.length; i++){
            result[i] = base.get(i);
        }
        return result;
    }
}
