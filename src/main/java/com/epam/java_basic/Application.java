package com.epam.java_basic;

import com.epam.java_basic.array_processor.ArrayProcessor;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.System.*;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {

    public static void main(String[] args) {
        Scanner sc = new Scanner(in);
        out.println("Enter the size of the array of random numbers:");
        int arrayLong = sc.nextInt();
        int[] array = new int[arrayLong];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) (Math.random()*(20+1)) - 10;
        }
        out.println("Original array:");
        out.println(Arrays.toString(array));//orig
        out.println("1) Exchange max negative and min positive elements:");
        out.println(Arrays.toString(ArrayProcessor.swapMaxNegativeAndMinPositiveElements(array)));//1
        out.println("2) Sum of elements on even positions:");
        out.println(ArrayProcessor.countSumOfElementsOnEvenPositions(array));//2
        out.println("3) Replace negative values with 0:");
        out.println(Arrays.toString(ArrayProcessor.replaceEachNegativeElementsWithZero(array)));//3
        out.println("4) Multiply by 3 each positive element standing before negative one:");
        out.println(Arrays.toString(ArrayProcessor.multiplyByThreeEachPositiveElementStandingBeforeNegative(array)));//4
        out.println("5) Difference between average and min element in array:");
        out.println(ArrayProcessor.calculateDifferenceBetweenAverageAndMinElement(array));//5
        out.println("6) Elements which present more than one time and stay on odd index:");
        out.println(Arrays.toString(ArrayProcessor.findSameElementsStandingOnOddPositions(array)));//6

    }

}
